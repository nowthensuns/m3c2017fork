"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown0(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    X = np.zeros((Nt+1,M))

    for i in range(M):
        for j in range(Nt):
            X[j+1,i] = X[j,i] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=1)
    Xv = np.var(X,axis=1)
    return X,Xm,Xv

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    N = np.sqrt(dt)*randn(Nt+1,M) #Generate all needed random numbers
    N[0,:] = 0. #Initial conditions, left out during lecture 5

    X = np.cumsum(N,axis=0) #Compute

    Xm = np.mean(X,axis=1)
    Xv = np.var(X,axis=1)
    return X,Xm,Xv


def analyze(Mvalues,Nt=100,display=False):
    """Calculates error in the variance at time t = 100 over different M values in Mvalues where dt = 1.
    Returns: Mvalues: original M values, Mvar: the calculated variances at t = 100,
    err: the error in the variance at t = 100
    """
    #Calculate the variance for the different values of M at t = 100
    Mvar = np.zeros_like(Mvalues)
    for i, M in enumerate(Mvalues):
        Mvar[i] = brown1(Nt,M)[2][-1]
    #Calculate the error at t = 100
    err = np.abs(Mvar-100)
    print(err)
    if display:
        fig = plt.figure()
        ax = plt.axes()
        plt.loglog(Mvalues,err)
        plt.ylabel("Variance error")
        plt.xlabel("M")
        plt.grid(True)
        plt.show()
    return Mvalues, Mvar, err


if __name__ == '__main__':
    #Test code
    Mvals = np.array([10,100,1000,10000])
    analyze(Mvals,display=True)
