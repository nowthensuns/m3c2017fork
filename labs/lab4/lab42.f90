! Lab 4, task 2

program task2
    implicit none
    real(kind=8) :: final_sum

    call computeSum(5,1, final_sum)
    print *, final_sum

end program task2

! To compile this code:
! $ gfortran -o task1.exe lab41.f90
! To run the resulting executable: $ ./task1.exe

subroutine computeSum(N, display_iteration, odd_sum)
  implicit none
  integer :: i1
  integer, intent(in) :: N, display_iteration
  real(kind=8), intent(out) :: odd_sum
  odd_sum = 0.d0
!Add a do-loop which sums the first N odd integers and prints the result
  do i1 = 1,N
    odd_sum = odd_sum + 2*i1 - 1
    if (display_iteration == 1) then
      print *, "Current sum:", odd_sum
    end if

  end do

end subroutine computeSum
