!Dev code for project, part 2: Simulate Pedestrian motion model
module part2
  implicit none
  ! integer :: isample
  ! real(kind=8), allocatable :: x(:,:),y(:,:)
contains

  subroutine simulate(m,nt,s0,l,d,a,x,y,psi)
    !Pedestrian motion model
    !input variables:
    !n =  m^2 = number of students
    !nt: number of time steps
    !s0: student speed
    !l: initial spacing between students
    !d: student motion influence by all other students within distance <= d
    !a: noise amplitude
    !output variables:
    !x,y: all m^2 student paths from i=1 to nt+1
    !psi: synchronization parameter, store at all nt+1 times (including initial condition)
    implicit none
    integer, intent(in) :: m,nt
    real(kind=8), intent(in) :: s0,l,d,a
    real(kind=8), dimension(m*m,nt+1), intent(out) :: x,y
    real(kind=8), dimension(nt+1), intent(out) :: psi
    integer :: i1,j1,k1


    ! initialize student positions
    x = 0.d0
    y = 0.d0
    k1 = 0
    do i1 = 1,m
      do j1=1,m
        k1 = k1 + 1
        x(k1,1) = (j1-1)*l/2 - (m-1)*l/4
        y(k1,1) = (i1-1)*l/2 - (m-1)*l/4
      end do
    end do
    x(:,1) = x(:,1)/(m-1)
    y(:,1) = y(:,1)/(m-1)


  end subroutine simulate


  subroutine simulate_omp(m,nt,s0,l,d,a,numthreads,x,y,psi)
    !Pedestrian motion model
    !input variables:
    !n = m^2 = number of students
    !nt: number of time steps
    !s0: student speed
    !l: initial spacing between students
    !d: student motion influence by all other students within distance <= d
    !a: noise amplitude
    !numthreads: number of threads to use in parallel regions
    !output variables:
    !x,y: all m^2 student paths from i=1 to nt+1
    !psi: synchronization parameter, store at all nt+1 times (including initial condition)
    use omp_lib
    implicit none
    integer, intent(in) :: m,nt,numthreads
    real(kind=8), intent(in) :: s0,l,d,a
    real(kind=8), dimension(m*m,nt+1), intent(out) :: x,y
    real(kind=8), dimension(nt+1), intent(out) :: psi



  end subroutine simulate_omp





end module part2
